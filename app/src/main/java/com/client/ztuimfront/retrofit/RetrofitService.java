package com.client.ztuimfront.retrofit;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    public Retrofit getRetrofit() {
        return retrofit;
    }

    private Retrofit retrofit;

    public RetrofitService() {
        initializeRetrofit();
    }

    private void initializeRetrofit() {

        //in case it isnt working change ip to ipv4 of your computer
    retrofit = new Retrofit.Builder().baseUrl("http://172.24.192.81:8080")
            .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
    }


}
